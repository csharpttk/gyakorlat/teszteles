﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tesztelendő;
namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {  
            string szó = "alma";
            bool eredmény = Program.Csupae(szó);
            Assert.AreEqual(false, eredmény);
        }
        [TestMethod]
        public void TestMethod2()
        {
            string szó = "elem";
            bool eredmény = Program.Csupae(szó);
            Assert.AreEqual(true, eredmény);
        }
        [TestMethod]
        public void TestMethod3()
        {
            string szó = "";
            bool eredmény = Program.Csupae(szó);
            Assert.AreEqual(true, eredmény);
        }
        [TestMethod]
        public void TestMethod4()
        {
            string szó = "erő";
            bool eredmény = Program.Csupae(szó);
            Assert.AreEqual(false, eredmény);
        }
    }
}

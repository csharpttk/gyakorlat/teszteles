﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tesztelendő
{
    public class Program
    {
        public static bool Csupae(string szó)
        {
            szó = szó.ToLower();
            string mgh = "aáéiíoóöőuúüű"; //ez e betű nincs benne...
            int i =0  ;
            while (i < mgh.Length && !szó.Contains(mgh[i]))
                i++;
            return i >= mgh.Length;
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Kérek egy szót!");
            string szó = Console.ReadLine();
            if (Csupae(szó)){
                Console.WriteLine("Eszperente a "+szó);
            }
            else
            {
                Console.WriteLine("Nem eszperente a " + szó);
            }
        }
    }
}
